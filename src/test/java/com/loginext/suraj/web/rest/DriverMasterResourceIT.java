package com.loginext.suraj.web.rest;

import com.loginext.suraj.LogiNextProgrammingChallengeApp;
import com.loginext.suraj.domain.DriverMaster;
import com.loginext.suraj.repository.DriverMasterRepository;
import com.loginext.suraj.service.DriverMasterService;
import com.loginext.suraj.service.dto.DriverMasterDTO;
import com.loginext.suraj.service.mapper.DriverMasterMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DriverMasterResource} REST controller.
 */
@SpringBootTest(classes = LogiNextProgrammingChallengeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class DriverMasterResourceIT {

    private static final String DEFAULT_DRIVER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DRIVER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENT_LATITUDE = "AAAAAAAAAA";
    private static final String UPDATED_CURRENT_LATITUDE = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENT_LONGITUDE = "AAAAAAAAAA";
    private static final String UPDATED_CURRENT_LONGITUDE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private DriverMasterRepository driverMasterRepository;

    @Autowired
    private DriverMasterMapper driverMasterMapper;

    @Autowired
    private DriverMasterService driverMasterService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDriverMasterMockMvc;

    private DriverMaster driverMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DriverMaster createEntity(EntityManager em) {
        DriverMaster driverMaster = new DriverMaster()
            .driverName(DEFAULT_DRIVER_NAME)
            .currentLatitude(DEFAULT_CURRENT_LATITUDE)
            .currentLongitude(DEFAULT_CURRENT_LONGITUDE)
            .status(DEFAULT_STATUS)
            .updatedOn(DEFAULT_UPDATED_ON);
        return driverMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DriverMaster createUpdatedEntity(EntityManager em) {
        DriverMaster driverMaster = new DriverMaster()
            .driverName(UPDATED_DRIVER_NAME)
            .currentLatitude(UPDATED_CURRENT_LATITUDE)
            .currentLongitude(UPDATED_CURRENT_LONGITUDE)
            .status(UPDATED_STATUS)
            .updatedOn(UPDATED_UPDATED_ON);
        return driverMaster;
    }

    @BeforeEach
    public void initTest() {
        driverMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createDriverMaster() throws Exception {
        int databaseSizeBeforeCreate = driverMasterRepository.findAll().size();
        // Create the DriverMaster
        DriverMasterDTO driverMasterDTO = driverMasterMapper.toDto(driverMaster);
        restDriverMasterMockMvc.perform(post("/api/driver-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(driverMasterDTO)))
            .andExpect(status().isCreated());

        // Validate the DriverMaster in the database
        List<DriverMaster> driverMasterList = driverMasterRepository.findAll();
        assertThat(driverMasterList).hasSize(databaseSizeBeforeCreate + 1);
        DriverMaster testDriverMaster = driverMasterList.get(driverMasterList.size() - 1);
        assertThat(testDriverMaster.getDriverName()).isEqualTo(DEFAULT_DRIVER_NAME);
        assertThat(testDriverMaster.getCurrentLatitude()).isEqualTo(DEFAULT_CURRENT_LATITUDE);
        assertThat(testDriverMaster.getCurrentLongitude()).isEqualTo(DEFAULT_CURRENT_LONGITUDE);
        assertThat(testDriverMaster.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testDriverMaster.getUpdatedOn()).isEqualTo(DEFAULT_UPDATED_ON);
    }

    @Test
    @Transactional
    public void createDriverMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = driverMasterRepository.findAll().size();

        // Create the DriverMaster with an existing ID
        driverMaster.setId(1L);
        DriverMasterDTO driverMasterDTO = driverMasterMapper.toDto(driverMaster);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDriverMasterMockMvc.perform(post("/api/driver-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(driverMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DriverMaster in the database
        List<DriverMaster> driverMasterList = driverMasterRepository.findAll();
        assertThat(driverMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDriverMasters() throws Exception {
        // Initialize the database
        driverMasterRepository.saveAndFlush(driverMaster);

        // Get all the driverMasterList
        restDriverMasterMockMvc.perform(get("/api/driver-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(driverMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].driverName").value(hasItem(DEFAULT_DRIVER_NAME)))
            .andExpect(jsonPath("$.[*].currentLatitude").value(hasItem(DEFAULT_CURRENT_LATITUDE)))
            .andExpect(jsonPath("$.[*].currentLongitude").value(hasItem(DEFAULT_CURRENT_LONGITUDE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].updatedOn").value(hasItem(DEFAULT_UPDATED_ON.toString())));
    }
    
    @Test
    @Transactional
    public void getDriverMaster() throws Exception {
        // Initialize the database
        driverMasterRepository.saveAndFlush(driverMaster);

        // Get the driverMaster
        restDriverMasterMockMvc.perform(get("/api/driver-masters/{id}", driverMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(driverMaster.getId().intValue()))
            .andExpect(jsonPath("$.driverName").value(DEFAULT_DRIVER_NAME))
            .andExpect(jsonPath("$.currentLatitude").value(DEFAULT_CURRENT_LATITUDE))
            .andExpect(jsonPath("$.currentLongitude").value(DEFAULT_CURRENT_LONGITUDE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.updatedOn").value(DEFAULT_UPDATED_ON.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingDriverMaster() throws Exception {
        // Get the driverMaster
        restDriverMasterMockMvc.perform(get("/api/driver-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDriverMaster() throws Exception {
        // Initialize the database
        driverMasterRepository.saveAndFlush(driverMaster);

        int databaseSizeBeforeUpdate = driverMasterRepository.findAll().size();

        // Update the driverMaster
        DriverMaster updatedDriverMaster = driverMasterRepository.findById(driverMaster.getId()).get();
        // Disconnect from session so that the updates on updatedDriverMaster are not directly saved in db
        em.detach(updatedDriverMaster);
        updatedDriverMaster
            .driverName(UPDATED_DRIVER_NAME)
            .currentLatitude(UPDATED_CURRENT_LATITUDE)
            .currentLongitude(UPDATED_CURRENT_LONGITUDE)
            .status(UPDATED_STATUS)
            .updatedOn(UPDATED_UPDATED_ON);
        DriverMasterDTO driverMasterDTO = driverMasterMapper.toDto(updatedDriverMaster);

        restDriverMasterMockMvc.perform(put("/api/driver-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(driverMasterDTO)))
            .andExpect(status().isOk());

        // Validate the DriverMaster in the database
        List<DriverMaster> driverMasterList = driverMasterRepository.findAll();
        assertThat(driverMasterList).hasSize(databaseSizeBeforeUpdate);
        DriverMaster testDriverMaster = driverMasterList.get(driverMasterList.size() - 1);
        assertThat(testDriverMaster.getDriverName()).isEqualTo(UPDATED_DRIVER_NAME);
        assertThat(testDriverMaster.getCurrentLatitude()).isEqualTo(UPDATED_CURRENT_LATITUDE);
        assertThat(testDriverMaster.getCurrentLongitude()).isEqualTo(UPDATED_CURRENT_LONGITUDE);
        assertThat(testDriverMaster.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDriverMaster.getUpdatedOn()).isEqualTo(UPDATED_UPDATED_ON);
    }

    @Test
    @Transactional
    public void updateNonExistingDriverMaster() throws Exception {
        int databaseSizeBeforeUpdate = driverMasterRepository.findAll().size();

        // Create the DriverMaster
        DriverMasterDTO driverMasterDTO = driverMasterMapper.toDto(driverMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDriverMasterMockMvc.perform(put("/api/driver-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(driverMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DriverMaster in the database
        List<DriverMaster> driverMasterList = driverMasterRepository.findAll();
        assertThat(driverMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDriverMaster() throws Exception {
        // Initialize the database
        driverMasterRepository.saveAndFlush(driverMaster);

        int databaseSizeBeforeDelete = driverMasterRepository.findAll().size();

        // Delete the driverMaster
        restDriverMasterMockMvc.perform(delete("/api/driver-masters/{id}", driverMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DriverMaster> driverMasterList = driverMasterRepository.findAll();
        assertThat(driverMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
