package com.loginext.suraj.web.rest;

import com.loginext.suraj.LogiNextProgrammingChallengeApp;
import com.loginext.suraj.domain.BookingMaster;
import com.loginext.suraj.repository.BookingMasterRepository;
import com.loginext.suraj.service.BookingMasterService;
import com.loginext.suraj.service.dto.BookingMasterDTO;
import com.loginext.suraj.service.mapper.BookingMasterMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BookingMasterResource} REST controller.
 */
@SpringBootTest(classes = LogiNextProgrammingChallengeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class BookingMasterResourceIT {

    private static final String DEFAULT_ORDER_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_ORDER_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_CUSTOMER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENT_LATITUDE = "AAAAAAAAAA";
    private static final String UPDATED_CURRENT_LATITUDE = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENT_LONGITUDE = "AAAAAAAAAA";
    private static final String UPDATED_CURRENT_LONGITUDE = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_ALLOCATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_ALLOCATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private BookingMasterRepository bookingMasterRepository;

    @Autowired
    private BookingMasterMapper bookingMasterMapper;

    @Autowired
    private BookingMasterService bookingMasterService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBookingMasterMockMvc;

    private BookingMaster bookingMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BookingMaster createEntity(EntityManager em) {
        BookingMaster bookingMaster = new BookingMaster()
            .orderNumber(DEFAULT_ORDER_NUMBER)
            .customerName(DEFAULT_CUSTOMER_NAME)
            .currentLatitude(DEFAULT_CURRENT_LATITUDE)
            .currentLongitude(DEFAULT_CURRENT_LONGITUDE)
            .updatedOn(DEFAULT_UPDATED_ON)
            .allocatedOn(DEFAULT_ALLOCATED_ON);
        return bookingMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BookingMaster createUpdatedEntity(EntityManager em) {
        BookingMaster bookingMaster = new BookingMaster()
            .orderNumber(UPDATED_ORDER_NUMBER)
            .customerName(UPDATED_CUSTOMER_NAME)
            .currentLatitude(UPDATED_CURRENT_LATITUDE)
            .currentLongitude(UPDATED_CURRENT_LONGITUDE)
            .updatedOn(UPDATED_UPDATED_ON)
            .allocatedOn(UPDATED_ALLOCATED_ON);
        return bookingMaster;
    }

    @BeforeEach
    public void initTest() {
        bookingMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createBookingMaster() throws Exception {
        int databaseSizeBeforeCreate = bookingMasterRepository.findAll().size();
        // Create the BookingMaster
        BookingMasterDTO bookingMasterDTO = bookingMasterMapper.toDto(bookingMaster);
        restBookingMasterMockMvc.perform(post("/api/booking-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bookingMasterDTO)))
            .andExpect(status().isCreated());

        // Validate the BookingMaster in the database
        List<BookingMaster> bookingMasterList = bookingMasterRepository.findAll();
        assertThat(bookingMasterList).hasSize(databaseSizeBeforeCreate + 1);
        BookingMaster testBookingMaster = bookingMasterList.get(bookingMasterList.size() - 1);
        assertThat(testBookingMaster.getOrderNumber()).isEqualTo(DEFAULT_ORDER_NUMBER);
        assertThat(testBookingMaster.getCustomerName()).isEqualTo(DEFAULT_CUSTOMER_NAME);
        assertThat(testBookingMaster.getCurrentLatitude()).isEqualTo(DEFAULT_CURRENT_LATITUDE);
        assertThat(testBookingMaster.getCurrentLongitude()).isEqualTo(DEFAULT_CURRENT_LONGITUDE);
        assertThat(testBookingMaster.getUpdatedOn()).isEqualTo(DEFAULT_UPDATED_ON);
        assertThat(testBookingMaster.getAllocatedOn()).isEqualTo(DEFAULT_ALLOCATED_ON);
    }

    @Test
    @Transactional
    public void createBookingMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bookingMasterRepository.findAll().size();

        // Create the BookingMaster with an existing ID
        bookingMaster.setId(1L);
        BookingMasterDTO bookingMasterDTO = bookingMasterMapper.toDto(bookingMaster);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBookingMasterMockMvc.perform(post("/api/booking-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bookingMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BookingMaster in the database
        List<BookingMaster> bookingMasterList = bookingMasterRepository.findAll();
        assertThat(bookingMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBookingMasters() throws Exception {
        // Initialize the database
        bookingMasterRepository.saveAndFlush(bookingMaster);

        // Get all the bookingMasterList
        restBookingMasterMockMvc.perform(get("/api/booking-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bookingMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].orderNumber").value(hasItem(DEFAULT_ORDER_NUMBER)))
            .andExpect(jsonPath("$.[*].customerName").value(hasItem(DEFAULT_CUSTOMER_NAME)))
            .andExpect(jsonPath("$.[*].currentLatitude").value(hasItem(DEFAULT_CURRENT_LATITUDE)))
            .andExpect(jsonPath("$.[*].currentLongitude").value(hasItem(DEFAULT_CURRENT_LONGITUDE)))
            .andExpect(jsonPath("$.[*].updatedOn").value(hasItem(DEFAULT_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].allocatedOn").value(hasItem(DEFAULT_ALLOCATED_ON.toString())));
    }
    
    @Test
    @Transactional
    public void getBookingMaster() throws Exception {
        // Initialize the database
        bookingMasterRepository.saveAndFlush(bookingMaster);

        // Get the bookingMaster
        restBookingMasterMockMvc.perform(get("/api/booking-masters/{id}", bookingMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(bookingMaster.getId().intValue()))
            .andExpect(jsonPath("$.orderNumber").value(DEFAULT_ORDER_NUMBER))
            .andExpect(jsonPath("$.customerName").value(DEFAULT_CUSTOMER_NAME))
            .andExpect(jsonPath("$.currentLatitude").value(DEFAULT_CURRENT_LATITUDE))
            .andExpect(jsonPath("$.currentLongitude").value(DEFAULT_CURRENT_LONGITUDE))
            .andExpect(jsonPath("$.updatedOn").value(DEFAULT_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.allocatedOn").value(DEFAULT_ALLOCATED_ON.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingBookingMaster() throws Exception {
        // Get the bookingMaster
        restBookingMasterMockMvc.perform(get("/api/booking-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBookingMaster() throws Exception {
        // Initialize the database
        bookingMasterRepository.saveAndFlush(bookingMaster);

        int databaseSizeBeforeUpdate = bookingMasterRepository.findAll().size();

        // Update the bookingMaster
        BookingMaster updatedBookingMaster = bookingMasterRepository.findById(bookingMaster.getId()).get();
        // Disconnect from session so that the updates on updatedBookingMaster are not directly saved in db
        em.detach(updatedBookingMaster);
        updatedBookingMaster
            .orderNumber(UPDATED_ORDER_NUMBER)
            .customerName(UPDATED_CUSTOMER_NAME)
            .currentLatitude(UPDATED_CURRENT_LATITUDE)
            .currentLongitude(UPDATED_CURRENT_LONGITUDE)
            .updatedOn(UPDATED_UPDATED_ON)
            .allocatedOn(UPDATED_ALLOCATED_ON);
        BookingMasterDTO bookingMasterDTO = bookingMasterMapper.toDto(updatedBookingMaster);

        restBookingMasterMockMvc.perform(put("/api/booking-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bookingMasterDTO)))
            .andExpect(status().isOk());

        // Validate the BookingMaster in the database
        List<BookingMaster> bookingMasterList = bookingMasterRepository.findAll();
        assertThat(bookingMasterList).hasSize(databaseSizeBeforeUpdate);
        BookingMaster testBookingMaster = bookingMasterList.get(bookingMasterList.size() - 1);
        assertThat(testBookingMaster.getOrderNumber()).isEqualTo(UPDATED_ORDER_NUMBER);
        assertThat(testBookingMaster.getCustomerName()).isEqualTo(UPDATED_CUSTOMER_NAME);
        assertThat(testBookingMaster.getCurrentLatitude()).isEqualTo(UPDATED_CURRENT_LATITUDE);
        assertThat(testBookingMaster.getCurrentLongitude()).isEqualTo(UPDATED_CURRENT_LONGITUDE);
        assertThat(testBookingMaster.getUpdatedOn()).isEqualTo(UPDATED_UPDATED_ON);
        assertThat(testBookingMaster.getAllocatedOn()).isEqualTo(UPDATED_ALLOCATED_ON);
    }

    @Test
    @Transactional
    public void updateNonExistingBookingMaster() throws Exception {
        int databaseSizeBeforeUpdate = bookingMasterRepository.findAll().size();

        // Create the BookingMaster
        BookingMasterDTO bookingMasterDTO = bookingMasterMapper.toDto(bookingMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBookingMasterMockMvc.perform(put("/api/booking-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bookingMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BookingMaster in the database
        List<BookingMaster> bookingMasterList = bookingMasterRepository.findAll();
        assertThat(bookingMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBookingMaster() throws Exception {
        // Initialize the database
        bookingMasterRepository.saveAndFlush(bookingMaster);

        int databaseSizeBeforeDelete = bookingMasterRepository.findAll().size();

        // Delete the bookingMaster
        restBookingMasterMockMvc.perform(delete("/api/booking-masters/{id}", bookingMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BookingMaster> bookingMasterList = bookingMasterRepository.findAll();
        assertThat(bookingMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
