package com.loginext.suraj.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.loginext.suraj.web.rest.TestUtil;

public class BookingMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BookingMaster.class);
        BookingMaster bookingMaster1 = new BookingMaster();
        bookingMaster1.setId(1L);
        BookingMaster bookingMaster2 = new BookingMaster();
        bookingMaster2.setId(bookingMaster1.getId());
        assertThat(bookingMaster1).isEqualTo(bookingMaster2);
        bookingMaster2.setId(2L);
        assertThat(bookingMaster1).isNotEqualTo(bookingMaster2);
        bookingMaster1.setId(null);
        assertThat(bookingMaster1).isNotEqualTo(bookingMaster2);
    }
}
