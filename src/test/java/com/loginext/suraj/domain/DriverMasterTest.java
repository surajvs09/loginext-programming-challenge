package com.loginext.suraj.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.loginext.suraj.web.rest.TestUtil;

public class DriverMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DriverMaster.class);
        DriverMaster driverMaster1 = new DriverMaster();
        driverMaster1.setId(1L);
        DriverMaster driverMaster2 = new DriverMaster();
        driverMaster2.setId(driverMaster1.getId());
        assertThat(driverMaster1).isEqualTo(driverMaster2);
        driverMaster2.setId(2L);
        assertThat(driverMaster1).isNotEqualTo(driverMaster2);
        driverMaster1.setId(null);
        assertThat(driverMaster1).isNotEqualTo(driverMaster2);
    }
}
