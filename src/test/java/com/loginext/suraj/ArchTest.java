package com.loginext.suraj;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.loginext.suraj");

        noClasses()
            .that()
            .resideInAnyPackage("com.loginext.suraj.service..")
            .or()
            .resideInAnyPackage("com.loginext.suraj.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..com.loginext.suraj.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
