package com.loginext.suraj.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DriverMasterMapperTest {

    private DriverMasterMapper driverMasterMapper;

    @BeforeEach
    public void setUp() {
        driverMasterMapper = new DriverMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(driverMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(driverMasterMapper.fromId(null)).isNull();
    }
}
