package com.loginext.suraj.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BookingMasterMapperTest {

    private BookingMasterMapper bookingMasterMapper;

    @BeforeEach
    public void setUp() {
        bookingMasterMapper = new BookingMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(bookingMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(bookingMasterMapper.fromId(null)).isNull();
    }
}
