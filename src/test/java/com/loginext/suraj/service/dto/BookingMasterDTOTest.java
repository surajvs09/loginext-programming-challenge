package com.loginext.suraj.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.loginext.suraj.web.rest.TestUtil;

public class BookingMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BookingMasterDTO.class);
        BookingMasterDTO bookingMasterDTO1 = new BookingMasterDTO();
        bookingMasterDTO1.setId(1L);
        BookingMasterDTO bookingMasterDTO2 = new BookingMasterDTO();
        assertThat(bookingMasterDTO1).isNotEqualTo(bookingMasterDTO2);
        bookingMasterDTO2.setId(bookingMasterDTO1.getId());
        assertThat(bookingMasterDTO1).isEqualTo(bookingMasterDTO2);
        bookingMasterDTO2.setId(2L);
        assertThat(bookingMasterDTO1).isNotEqualTo(bookingMasterDTO2);
        bookingMasterDTO1.setId(null);
        assertThat(bookingMasterDTO1).isNotEqualTo(bookingMasterDTO2);
    }
}
