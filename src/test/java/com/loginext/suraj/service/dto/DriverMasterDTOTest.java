package com.loginext.suraj.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.loginext.suraj.web.rest.TestUtil;

public class DriverMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DriverMasterDTO.class);
        DriverMasterDTO driverMasterDTO1 = new DriverMasterDTO();
        driverMasterDTO1.setId(1L);
        DriverMasterDTO driverMasterDTO2 = new DriverMasterDTO();
        assertThat(driverMasterDTO1).isNotEqualTo(driverMasterDTO2);
        driverMasterDTO2.setId(driverMasterDTO1.getId());
        assertThat(driverMasterDTO1).isEqualTo(driverMasterDTO2);
        driverMasterDTO2.setId(2L);
        assertThat(driverMasterDTO1).isNotEqualTo(driverMasterDTO2);
        driverMasterDTO1.setId(null);
        assertThat(driverMasterDTO1).isNotEqualTo(driverMasterDTO2);
    }
}
