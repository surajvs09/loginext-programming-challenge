package com.loginext.suraj.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * A DriverMaster.
 */
@Entity
@Table(name = "driver_master")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class DriverMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "driver_name")
    private String driverName;

    @Column(name = "current_latitude")
    private String currentLatitude;

    @Column(name = "current_longitude")
    private String currentLongitude;

    @Column(name = "customer_name")
    private String customerName;

    @Column(name = "status")
    private String status;

    @Column(name = "updated_on")
    private Instant updatedOn;

    @Column(name = "distance", insertable = false, updatable = false)
    private Float distance;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDriverName() {
        return driverName;
    }

    public DriverMaster driverName(String driverName) {
        this.driverName = driverName;
        return this;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getCurrentLatitude() {
        return currentLatitude;
    }

    public DriverMaster currentLatitude(String currentLatitude) {
        this.currentLatitude = currentLatitude;
        return this;
    }

    public void setCurrentLatitude(String currentLatitude) {
        this.currentLatitude = currentLatitude;
    }

    public String getCurrentLongitude() {
        return currentLongitude;
    }

    public DriverMaster currentLongitude(String currentLongitude) {
        this.currentLongitude = currentLongitude;
        return this;
    }

    public void setCurrentLongitude(String currentLongitude) {
        this.currentLongitude = currentLongitude;
    }

    public String getStatus() {
        return status;
    }

    public DriverMaster status(String status) {
        this.status = status;
        return this;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Instant getUpdatedOn() {
        return updatedOn;
    }

    public DriverMaster updatedOn(Instant updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }

    public void setUpdatedOn(Instant updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DriverMaster)) {
            return false;
        }
        return id != null && id.equals(((DriverMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore


    @Override
    public String toString() {
        return "DriverMaster{" +
            "id=" + id +
            ", driverName='" + driverName + '\'' +
            ", currentLatitude='" + currentLatitude + '\'' +
            ", currentLongitude='" + currentLongitude + '\'' +
            ", customerName='" + customerName + '\'' +
            ", status='" + status + '\'' +
            ", updatedOn=" + updatedOn +
            ", distance=" + distance +
            '}';
    }
}
