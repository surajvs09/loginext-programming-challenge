package com.loginext.suraj.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * A BookingMaster.
 */
@Entity
@Table(name = "booking_master")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class BookingMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "order_number")
    private String orderNumber;

    @Column(name = "customer_name")
    private String customerName;

    @Column(name = "current_latitude")
    private String currentLatitude;

    @Column(name = "current_longitude")
    private String currentLongitude;

    @Column(name = "updated_on")
    private Instant updatedOn;

    @Column(name = "allocated_on")
    private Instant allocatedOn;

    @ManyToOne
    @JsonIgnoreProperties(value = "bookingMasters", allowSetters = true)
    private DriverMaster allocatedDriver;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public BookingMaster orderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public BookingMaster customerName(String customerName) {
        this.customerName = customerName;
        return this;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCurrentLatitude() {
        return currentLatitude;
    }

    public BookingMaster currentLatitude(String currentLatitude) {
        this.currentLatitude = currentLatitude;
        return this;
    }

    public void setCurrentLatitude(String currentLatitude) {
        this.currentLatitude = currentLatitude;
    }

    public String getCurrentLongitude() {
        return currentLongitude;
    }

    public BookingMaster currentLongitude(String currentLongitude) {
        this.currentLongitude = currentLongitude;
        return this;
    }

    public void setCurrentLongitude(String currentLongitude) {
        this.currentLongitude = currentLongitude;
    }

    public Instant getUpdatedOn() {
        return updatedOn;
    }

    public BookingMaster updatedOn(Instant updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }

    public void setUpdatedOn(Instant updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Instant getAllocatedOn() {
        return allocatedOn;
    }

    public BookingMaster allocatedOn(Instant allocatedOn) {
        this.allocatedOn = allocatedOn;
        return this;
    }

    public void setAllocatedOn(Instant allocatedOn) {
        this.allocatedOn = allocatedOn;
    }

    public DriverMaster getAllocatedDriver() {
        return allocatedDriver;
    }

    public BookingMaster allocatedDriver(DriverMaster driverMaster) {
        this.allocatedDriver = driverMaster;
        return this;
    }

    public void setAllocatedDriver(DriverMaster driverMaster) {
        this.allocatedDriver = driverMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BookingMaster)) {
            return false;
        }
        return id != null && id.equals(((BookingMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BookingMaster{" +
            "id=" + getId() +
            ", orderNumber='" + getOrderNumber() + "'" +
            ", customerName='" + getCustomerName() + "'" +
            ", currentLatitude='" + getCurrentLatitude() + "'" +
            ", currentLongitude='" + getCurrentLongitude() + "'" +
            ", updatedOn='" + getUpdatedOn() + "'" +
            ", allocatedOn='" + getAllocatedOn() + "'" +
            "}";
    }
}
