package com.loginext.suraj.web.rest;

import com.loginext.suraj.domain.DriverMaster;
import com.loginext.suraj.service.BookingMasterService;
import com.loginext.suraj.service.DriverMasterService;
import com.loginext.suraj.service.dto.BookingMasterDTO;
import com.loginext.suraj.service.dto.BookingRequestDTO;
import com.loginext.suraj.service.dto.DriverMasterDTO;
import com.loginext.suraj.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class BookingRequestResource {

    private final Logger log = LoggerFactory.getLogger(BookingRequestResource.class);

    private static final String ENTITY_NAME = "logiNextProgrammingChallengeBookingMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BookingMasterService bookingMasterService;

    private final DriverMasterService driverMasterService;

    public BookingRequestResource(BookingMasterService bookingMasterService, DriverMasterService driverMasterService) {
        this.bookingMasterService = bookingMasterService;
        this.driverMasterService = driverMasterService;
    }

    @PostMapping("/find/booking")
    public ResponseEntity<List<DriverMasterDTO>> findBooking(@RequestBody BookingRequestDTO bookingRequestDTO) throws URISyntaxException {
        log.debug("REST request to save BookingRequestDTO : {}", bookingRequestDTO);
        if (bookingRequestDTO == null || !bookingRequestDTO.isValid()) {
            throw new BadRequestAlertException("BookingRequestDTO invalid", ENTITY_NAME, "invalid");
        }
        List<DriverMasterDTO> bookingMasterDTOList = driverMasterService.findByPoint(bookingRequestDTO);
        return ResponseEntity.ok().body(bookingMasterDTOList);
    }
}
