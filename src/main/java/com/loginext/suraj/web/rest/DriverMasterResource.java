package com.loginext.suraj.web.rest;

import com.loginext.suraj.service.BookingMasterService;
import com.loginext.suraj.service.DriverMasterService;
import com.loginext.suraj.service.dto.BookingMasterDTO;
import com.loginext.suraj.service.dto.DriverMasterDTO;
import com.loginext.suraj.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.loginext.suraj.domain.DriverMaster}.
 */
@RestController
@RequestMapping("/api")
public class DriverMasterResource {

    private final Logger log = LoggerFactory.getLogger(DriverMasterResource.class);

    private static final String ENTITY_NAME = "logiNextProgrammingChallengeDriverMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DriverMasterService driverMasterService;

    private final BookingMasterService bookingMasterService;

    public DriverMasterResource(DriverMasterService driverMasterService, BookingMasterService bookingMasterService) {
        this.driverMasterService = driverMasterService;
        this.bookingMasterService = bookingMasterService;
    }

    /**
     * {@code POST  /driver-masters} : Create a new driverMaster.
     *
     * @param driverMasterDTO the driverMasterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new driverMasterDTO, or with status {@code 400 (Bad Request)} if the driverMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/driver-masters")
    public ResponseEntity<DriverMasterDTO> createDriverMaster(@RequestBody DriverMasterDTO driverMasterDTO) throws URISyntaxException {
        log.debug("REST request to save DriverMaster : {}", driverMasterDTO);
        if (driverMasterDTO.getId() != null) {
            throw new BadRequestAlertException("A new driverMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DriverMasterDTO result = driverMasterService.save(driverMasterDTO);
        return ResponseEntity.created(new URI("/api/driver-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/book/driver")
    public ResponseEntity<BookingMasterDTO> bookDriverMaster(@RequestBody DriverMasterDTO driverMasterDTO) throws URISyntaxException {
        log.debug("REST request to book DriverMaster : {}", driverMasterDTO);
        if (driverMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DriverMasterDTO masterDTO = driverMasterService.save(driverMasterDTO.now());

        BookingMasterDTO result = bookingMasterService.save(masterDTO.toBooking());

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/driver-masters")
    public ResponseEntity<DriverMasterDTO> updateDriverMaster(@RequestBody DriverMasterDTO driverMasterDTO) throws URISyntaxException {
        log.debug("REST request to update DriverMaster : {}", driverMasterDTO);
        if (driverMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DriverMasterDTO result = driverMasterService.save(driverMasterDTO);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, driverMasterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /driver-masters} : get all the driverMasters.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of driverMasters in body.
     */
    @GetMapping("/driver-masters")
    public ResponseEntity<List<DriverMasterDTO>> getAllDriverMasters(Pageable pageable) {
        log.debug("REST request to get a page of DriverMasters");
        Page<DriverMasterDTO> page = driverMasterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /driver-masters/:id} : get the "id" driverMaster.
     *
     * @param id the id of the driverMasterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the driverMasterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/driver-masters/{id}")
    public ResponseEntity<DriverMasterDTO> getDriverMaster(@PathVariable Long id) {
        log.debug("REST request to get DriverMaster : {}", id);
        Optional<DriverMasterDTO> driverMasterDTO = driverMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(driverMasterDTO);
    }

    /**
     * {@code DELETE  /driver-masters/:id} : delete the "id" driverMaster.
     *
     * @param id the id of the driverMasterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/driver-masters/{id}")
    public ResponseEntity<Void> deleteDriverMaster(@PathVariable Long id) {
        log.debug("REST request to delete DriverMaster : {}", id);
        driverMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
