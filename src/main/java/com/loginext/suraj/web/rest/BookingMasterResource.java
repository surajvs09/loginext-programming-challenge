package com.loginext.suraj.web.rest;

import com.loginext.suraj.service.BookingMasterService;
import com.loginext.suraj.service.dto.BookingMasterDTO;
import com.loginext.suraj.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.loginext.suraj.domain.BookingMaster}.
 */
@RestController
@RequestMapping("/api")
public class BookingMasterResource {

    private final Logger log = LoggerFactory.getLogger(BookingMasterResource.class);

    private static final String ENTITY_NAME = "logiNextProgrammingChallengeBookingMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BookingMasterService bookingMasterService;

    public BookingMasterResource(BookingMasterService bookingMasterService) {
        this.bookingMasterService = bookingMasterService;
    }

    /**
     * {@code POST  /booking-masters} : Create a new bookingMaster.
     *
     * @param bookingMasterDTO the bookingMasterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bookingMasterDTO, or with status {@code 400 (Bad Request)} if the bookingMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/booking-masters")
    public ResponseEntity<BookingMasterDTO> createBookingMaster(@RequestBody BookingMasterDTO bookingMasterDTO) throws URISyntaxException {
        log.debug("REST request to save BookingMaster : {}", bookingMasterDTO);
        if (bookingMasterDTO.getId() != null) {
            throw new BadRequestAlertException("A new bookingMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BookingMasterDTO result = bookingMasterService.save(bookingMasterDTO);
        return ResponseEntity.created(new URI("/api/booking-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /booking-masters} : Updates an existing bookingMaster.
     *
     * @param bookingMasterDTO the bookingMasterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bookingMasterDTO,
     * or with status {@code 400 (Bad Request)} if the bookingMasterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bookingMasterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/booking-masters")
    public ResponseEntity<BookingMasterDTO> updateBookingMaster(@RequestBody BookingMasterDTO bookingMasterDTO) throws URISyntaxException {
        log.debug("REST request to update BookingMaster : {}", bookingMasterDTO);
        if (bookingMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BookingMasterDTO result = bookingMasterService.save(bookingMasterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, bookingMasterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /booking-masters} : get all the bookingMasters.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bookingMasters in body.
     */
    @GetMapping("/booking-masters")
    public ResponseEntity<List<BookingMasterDTO>> getAllBookingMasters(Pageable pageable) {
        log.debug("REST request to get a page of BookingMasters");
        Page<BookingMasterDTO> page = bookingMasterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /booking-masters/:id} : get the "id" bookingMaster.
     *
     * @param id the id of the bookingMasterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bookingMasterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/booking-masters/{id}")
    public ResponseEntity<BookingMasterDTO> getBookingMaster(@PathVariable Long id) {
        log.debug("REST request to get BookingMaster : {}", id);
        Optional<BookingMasterDTO> bookingMasterDTO = bookingMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bookingMasterDTO);
    }

    /**
     * {@code DELETE  /booking-masters/:id} : delete the "id" bookingMaster.
     *
     * @param id the id of the bookingMasterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/booking-masters/{id}")
    public ResponseEntity<Void> deleteBookingMaster(@PathVariable Long id) {
        log.debug("REST request to delete BookingMaster : {}", id);
        bookingMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
