/**
 * View Models used by Spring MVC REST controllers.
 */
package com.loginext.suraj.web.rest.vm;
