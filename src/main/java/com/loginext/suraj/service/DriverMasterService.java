package com.loginext.suraj.service;

import com.loginext.suraj.service.dto.BookingMasterDTO;
import com.loginext.suraj.service.dto.BookingRequestDTO;
import com.loginext.suraj.service.dto.DriverMasterDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.loginext.suraj.domain.DriverMaster}.
 */
public interface DriverMasterService {

    /**
     * Save a driverMaster.
     *
     * @param driverMasterDTO the entity to save.
     * @return the persisted entity.
     */
    DriverMasterDTO save(DriverMasterDTO driverMasterDTO);

    /**
     * Get all the driverMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DriverMasterDTO> findAll(Pageable pageable);


    /**
     * Get the "id" driverMaster.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DriverMasterDTO> findOne(Long id);

    /**
     * Delete the "id" driverMaster.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<DriverMasterDTO> findByPoint(BookingRequestDTO bookingRequestDTO);
}
