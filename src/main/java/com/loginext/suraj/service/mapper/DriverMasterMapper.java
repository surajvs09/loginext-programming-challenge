package com.loginext.suraj.service.mapper;


import com.loginext.suraj.domain.DriverMaster;
import com.loginext.suraj.service.dto.DriverMasterDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link DriverMaster} and its DTO {@link DriverMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DriverMasterMapper extends EntityMapper<DriverMasterDTO, DriverMaster> {



    default DriverMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        DriverMaster driverMaster = new DriverMaster();
        driverMaster.setId(id);
        return driverMaster;
    }
}
