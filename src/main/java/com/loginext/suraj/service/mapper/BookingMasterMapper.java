package com.loginext.suraj.service.mapper;


import com.loginext.suraj.domain.BookingMaster;
import com.loginext.suraj.service.dto.BookingMasterDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link BookingMaster} and its DTO {@link BookingMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {DriverMasterMapper.class})
public interface BookingMasterMapper extends EntityMapper<BookingMasterDTO, BookingMaster> {

    @Mapping(source = "allocatedDriver.id", target = "allocatedDriverId")
    BookingMasterDTO toDto(BookingMaster bookingMaster);

    @Mapping(source = "allocatedDriverId", target = "allocatedDriver")
    BookingMaster toEntity(BookingMasterDTO bookingMasterDTO);

    default BookingMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        BookingMaster bookingMaster = new BookingMaster();
        bookingMaster.setId(id);
        return bookingMaster;
    }
}
