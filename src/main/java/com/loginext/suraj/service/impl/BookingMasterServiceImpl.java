package com.loginext.suraj.service.impl;

import com.loginext.suraj.domain.BookingMaster;
import com.loginext.suraj.repository.BookingMasterRepository;
import com.loginext.suraj.service.BookingMasterService;
import com.loginext.suraj.service.dto.BookingMasterDTO;
import com.loginext.suraj.service.mapper.BookingMasterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BookingMaster}.
 */
@Service
@Transactional
public class BookingMasterServiceImpl implements BookingMasterService {

    private final Logger log = LoggerFactory.getLogger(BookingMasterServiceImpl.class);

    private final BookingMasterRepository bookingMasterRepository;

    private final BookingMasterMapper bookingMasterMapper;

    public BookingMasterServiceImpl(BookingMasterRepository bookingMasterRepository, BookingMasterMapper bookingMasterMapper) {
        this.bookingMasterRepository = bookingMasterRepository;
        this.bookingMasterMapper = bookingMasterMapper;
    }

    @Override
    public BookingMasterDTO save(BookingMasterDTO bookingMasterDTO) {
        log.debug("Request to save BookingMaster : {}", bookingMasterDTO);
        BookingMaster bookingMaster = bookingMasterMapper.toEntity(bookingMasterDTO);
        bookingMaster = bookingMasterRepository.save(bookingMaster);
        return bookingMasterMapper.toDto(bookingMaster);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BookingMasterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BookingMasters");
        return bookingMasterRepository.findAll(pageable)
            .map(bookingMasterMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<BookingMasterDTO> findOne(Long id) {
        log.debug("Request to get BookingMaster : {}", id);
        return bookingMasterRepository.findById(id)
            .map(bookingMasterMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete BookingMaster : {}", id);
        bookingMasterRepository.deleteById(id);
    }
}
