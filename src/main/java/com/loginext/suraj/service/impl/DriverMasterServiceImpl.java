package com.loginext.suraj.service.impl;

import com.loginext.suraj.domain.DriverMaster;
import com.loginext.suraj.repository.DriverMasterRepository;
import com.loginext.suraj.service.DriverMasterService;
import com.loginext.suraj.service.dto.BookingMasterDTO;
import com.loginext.suraj.service.dto.BookingRequestDTO;
import com.loginext.suraj.service.dto.DriverMasterDTO;
import com.loginext.suraj.service.mapper.DriverMasterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link DriverMaster}.
 */
@Service
@Transactional
public class DriverMasterServiceImpl implements DriverMasterService {

    private final Logger log = LoggerFactory.getLogger(DriverMasterServiceImpl.class);

    private final DriverMasterRepository driverMasterRepository;

    private final DriverMasterMapper driverMasterMapper;

    public DriverMasterServiceImpl(DriverMasterRepository driverMasterRepository, DriverMasterMapper driverMasterMapper) {
        this.driverMasterRepository = driverMasterRepository;
        this.driverMasterMapper = driverMasterMapper;
    }

    @Override
    public DriverMasterDTO save(DriverMasterDTO driverMasterDTO) {
        log.debug("Request to save DriverMaster : {}", driverMasterDTO);
        DriverMaster driverMaster = driverMasterMapper.toEntity(driverMasterDTO);
        driverMaster = driverMasterRepository.save(driverMaster);
        return driverMasterMapper.toDto(driverMaster);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DriverMasterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DriverMasters");
        return driverMasterRepository.findAll(pageable)
            .map(driverMasterMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<DriverMasterDTO> findOne(Long id) {
        log.debug("Request to get DriverMaster : {}", id);
        return driverMasterRepository.findById(id)
            .map(driverMasterMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DriverMaster : {}", id);
        driverMasterRepository.deleteById(id);
    }

    @Override
    public List<DriverMasterDTO> findByPoint(BookingRequestDTO bookingRequestDTO) {
        log.debug("Request to get BookingRequestDTO : {}", bookingRequestDTO);
        return driverMasterRepository.findByPoint(bookingRequestDTO.getCurrentLatitude(), bookingRequestDTO.getCurrentLongitude()).stream().map(driverMasterMapper::toDto).collect(Collectors.toList());
    }
}
