package com.loginext.suraj.service;

import com.loginext.suraj.service.dto.BookingMasterDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.loginext.suraj.domain.BookingMaster}.
 */
public interface BookingMasterService {

    /**
     * Save a bookingMaster.
     *
     * @param bookingMasterDTO the entity to save.
     * @return the persisted entity.
     */
    BookingMasterDTO save(BookingMasterDTO bookingMasterDTO);

    /**
     * Get all the bookingMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BookingMasterDTO> findAll(Pageable pageable);


    /**
     * Get the "id" bookingMaster.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BookingMasterDTO> findOne(Long id);

    /**
     * Delete the "id" bookingMaster.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
