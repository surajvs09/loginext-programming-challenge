package com.loginext.suraj.service.dto;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.io.Serializable;
import java.time.Instant;

public class DriverMasterDTO implements Serializable {

    private Long id;

    private String driverName;

    private String customerName;

    private String currentLatitude;

    private String currentLongitude;

    private String status;

    private Instant updatedOn;

    private Float distance;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCurrentLatitude() {
        return currentLatitude;
    }

    public void setCurrentLatitude(String currentLatitude) {
        this.currentLatitude = currentLatitude;
    }

    public String getCurrentLongitude() {
        return currentLongitude;
    }

    public void setCurrentLongitude(String currentLongitude) {
        this.currentLongitude = currentLongitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Instant getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Instant updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DriverMasterDTO)) {
            return false;
        }

        return id != null && id.equals(((DriverMasterDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore


    @Override
    public String toString() {
        return "DriverMasterDTO{" +
            "id=" + id +
            ", driverName='" + driverName + '\'' +
            ", customerName='" + customerName + '\'' +
            ", currentLatitude='" + currentLatitude + '\'' +
            ", currentLongitude='" + currentLongitude + '\'' +
            ", status='" + status + '\'' +
            ", updatedOn=" + updatedOn +
            ", distance=" + distance +
            '}';
    }

    public DriverMasterDTO now() {
        this.updatedOn = Instant.now();
        return this;
    }

    public BookingMasterDTO toBooking() {
        BookingMasterDTO bookingMasterDTO  = new BookingMasterDTO();
        bookingMasterDTO.setAllocatedDriverId(this.id);
        bookingMasterDTO.setAllocatedOn(Instant.now());
        bookingMasterDTO.setCurrentLatitude(this.currentLatitude);
        bookingMasterDTO.setCurrentLongitude(this.currentLongitude);
        bookingMasterDTO.setCustomerName(this.customerName);
        bookingMasterDTO.setOrderNumber(RandomStringUtils.randomNumeric(8).toUpperCase());
        bookingMasterDTO.setUpdatedOn(Instant.now());
        return bookingMasterDTO;
    }
}
