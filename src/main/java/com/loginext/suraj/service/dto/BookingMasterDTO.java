package com.loginext.suraj.service.dto;

import java.io.Serializable;
import java.time.Instant;

/**
 * A DTO for the {@link com.loginext.suraj.domain.BookingMaster} entity.
 */
public class BookingMasterDTO implements Serializable {

    private Long id;

    private String orderNumber;

    private String customerName;

    private String currentLatitude;

    private String currentLongitude;

    private Instant updatedOn;

    private Instant allocatedOn;


    private Long allocatedDriverId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCurrentLatitude() {
        return currentLatitude;
    }

    public void setCurrentLatitude(String currentLatitude) {
        this.currentLatitude = currentLatitude;
    }

    public String getCurrentLongitude() {
        return currentLongitude;
    }

    public void setCurrentLongitude(String currentLongitude) {
        this.currentLongitude = currentLongitude;
    }

    public Instant getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Instant updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Instant getAllocatedOn() {
        return allocatedOn;
    }

    public void setAllocatedOn(Instant allocatedOn) {
        this.allocatedOn = allocatedOn;
    }

    public Long getAllocatedDriverId() {
        return allocatedDriverId;
    }

    public void setAllocatedDriverId(Long driverMasterId) {
        this.allocatedDriverId = driverMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BookingMasterDTO)) {
            return false;
        }

        return id != null && id.equals(((BookingMasterDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BookingMasterDTO{" +
            "id=" + getId() +
            ", orderNumber='" + getOrderNumber() + "'" +
            ", customerName='" + getCustomerName() + "'" +
            ", currentLatitude='" + getCurrentLatitude() + "'" +
            ", currentLongitude='" + getCurrentLongitude() + "'" +
            ", updatedOn='" + getUpdatedOn() + "'" +
            ", allocatedOn='" + getAllocatedOn() + "'" +
            ", allocatedDriverId=" + getAllocatedDriverId() +
            "}";
    }
}
