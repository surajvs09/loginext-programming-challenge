package com.loginext.suraj.service.dto;

import java.io.Serializable;
import java.time.Instant;

public class BookingRequestDTO implements Serializable {


    private String customerName;

    private Float currentLatitude;

    private Float currentLongitude;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Float getCurrentLatitude() {
        return currentLatitude;
    }

    public void setCurrentLatitude(Float currentLatitude) {
        this.currentLatitude = currentLatitude;
    }

    public Float getCurrentLongitude() {
        return currentLongitude;
    }

    public void setCurrentLongitude(Float currentLongitude) {
        this.currentLongitude = currentLongitude;
    }

    public boolean isValid() {
        return this.currentLatitude != null && this.currentLongitude != null && this.customerName != null;
    }
}
