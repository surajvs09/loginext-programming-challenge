package com.loginext.suraj.repository;

import com.google.common.io.Files;
import com.loginext.suraj.domain.DriverMaster;
import com.loginext.suraj.service.dto.BookingRequestDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the DriverMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DriverMasterRepository extends JpaRepository<DriverMaster, Long> {

    @Query(value = QueryConstant.FIND_CLOSEST_DRIVER, nativeQuery = true)
    List<DriverMaster> findByPoint(@Param("userLatitude") Float userLatitude, @Param("userLongitude") Float userLongitude);
}
