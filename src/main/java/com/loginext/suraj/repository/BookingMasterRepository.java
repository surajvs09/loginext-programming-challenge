package com.loginext.suraj.repository;

import com.loginext.suraj.domain.BookingMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BookingMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BookingMasterRepository extends JpaRepository<BookingMaster, Long> {
}
