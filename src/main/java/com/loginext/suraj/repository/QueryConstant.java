package com.loginext.suraj.repository;

public class QueryConstant {
    public static final String FIND_CLOSEST_DRIVER = "SELECT d.id, d.driver_name, d.customer_name, d.current_latitude, d.current_longitude, d.status, d.updated_on, ( 6371 * acos(cos(radians( :userLatitude)) * cos(radians(`current_latitude`)) * cos(radians(`current_longitude`) - radians(:userLongitude)) + sin(radians(:userLatitude)) * sin(radians(`current_latitude` ))) ) AS distance FROM driver_master d ORDER BY distance";
}
