$( document ).ready(function() {
  let host = 'http://localhost:8080';
  let markers = [];

  function fetch(payload, success) {
    $.ajax({
          type: 'POST',
          url: host + '/api/find/booking',
          data: JSON.stringify (payload),
          success: function(data) { success(data) },
          contentType: "application/json",
          dataType: 'json'
      });
  }

   function upload(payload) {
      $.ajax({
            type: 'PUT',
            url: host + '/api/book/driver',
            data: JSON.stringify (payload),
            success: function(data) { bookingConfirmation(data) },
            contentType: "application/json",
            dataType: 'json'
        });
    }

  function bookingConfirmation(booking) {
    alert("Booking request processed successfully. Order Number : " + booking.orderNumber);
    findDriverByPoint();
  }

  function fetchDriverList(centerLocation) {
      let payload = {currentLatitude: centerLocation.lat(), currentLongitude: centerLocation.lng(), customerName:''};
      fetch(payload, drawDriverMarkers);

  }

  function addMarker(point) {
    var title = 'Name : '.concat(point.driverName);
    if (point.distance) {
      title = title.concat(' | Distance : ').concat(Math.round((point.distance + Number.EPSILON) * 100) / 100).concat(' kms approx.');
    }
    var url = 'http://maps.google.com/mapfiles/ms/icons/' + (point.status == 'Available' ? 'green' : 'purple') + '.png';
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(point.currentLatitude, point.currentLongitude),
        title: title,
        animation: google.maps.Animation.DROP,
        draggable: false,
        icon: {
          url: url
        },
        map: map
    });
    markers.push(marker);
  }

  function drawDriverMarkers(data) {
    for (driver of data) {
        addMarker(driver);
    }
  }

  var logiNextPoint  = new google.maps.LatLng(19.1115453, 72.9070646);
  fetchDriverList(logiNextPoint);

  var showPosition = function(resetPoint) {
      var marker = new google.maps.Marker({
          position: resetPoint ? resetPoint : logiNextPoint,
          title: 'Starting point location',
          animation: google.maps.Animation.DROP,
          draggable: true,
          icon: {
            url: 'http://maps.google.com/mapfiles/ms/icons/pink.png'
          },
          map: map
      });
      map.setCenter(marker.getPosition());
      markers.push(marker);
  }
  navigator.geolocation.getCurrentPosition(showPosition);
  map = new google.maps.Map(document.getElementById("map_go"), { zoom: 13,center: logiNextPoint });
  showPosition();


  function attachEvent() {
    $(".book-driver").unbind().on('click', function(event) {
        event.stopPropagation();
        event.stopImmediatePropagation();
        var bookingRequest = JSON.parse(atob($(this).data('value')));
        bookingRequest.customerName = $("input[name=customerName]").val();
        bookingRequest.status = 'Busy';
        bookingRequest.distance = undefined;
        upload(bookingRequest);
    });
  }

  $("form.find-booking").submit(function(e){
      e.preventDefault();
      findDriverByPoint();
  });

  function findDriverByPoint() {
    var customerName = $("input[name=customerName]").val();
    var currentLatitude =  parseFloat($("input[name=currentLatitude]").val());
    var currentLongitude = parseFloat($("input[name=currentLongitude]").val());

    let payload = {currentLatitude: currentLatitude, currentLongitude: currentLongitude, customerName:''};showPosition();
    deleteMarkers();
    fetch(payload, showDriversForBooking);
    showPosition(new google.maps.LatLng(currentLatitude, currentLongitude));
  }

  function showDriversForBooking(data) {
    drawDriverMarkers(data);
    loadResults(data);
  }

  function setMapOnAll(map) {
    for (let i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }

  function clearMarkers() {
    setMapOnAll(null);
  }

  function deleteMarkers() {
    clearMarkers();
    markers = [];
  }

  function loadResults(data) {
    var table = $("#results tbody");
    table.empty();
    var index=1;
    for (driver of data) {
        table.append(newRowContent(index, driver));
        ++index;
    }
    attachEvent();
  }
  function newRowContent(index, driver) {
      var row = '<tr>' +
                   '<td> ' + index +' </td>' +
                   '<td>' + driver.driverName + '</td>' +
                   '<td>' + driver.distance + '</td>' +
                   '<td>' + (driver.customerName != null ? driver.customerName : '') + '</td>' +
                   '<td>' + driver.status + '</td>' ;
                   if (driver.status == 'Available') row = row + '<td><input class="book-driver" type="button" data-value="'+ btoa(JSON.stringify(driver)) + '" value="Book"></td>' ;
                   else row = row + '<td></td>'
           row+='</tr>';
      return row;
  }

});

