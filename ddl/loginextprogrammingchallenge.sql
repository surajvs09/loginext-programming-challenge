--
-- Database: `loginextprogrammingchallenge`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking_master`
--

CREATE TABLE `booking_master` (
  `id` bigint(20) NOT NULL,
  `order_number` varchar(255) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `current_latitude` varchar(255) DEFAULT NULL,
  `current_longitude` varchar(255) DEFAULT NULL,
  `updated_on` datetime,
  `allocated_on` datetime,
  `allocated_driver_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_master`
--

INSERT INTO `booking_master` (`id`, `order_number`, `customer_name`, `current_latitude`, `current_longitude`, `updated_on`, `allocated_on`, `allocated_driver_id`) VALUES
(12, '34121345', 'Suraj Shingade', '19.137367', '72.852037', '2020-10-13 09:11:41', '2020-10-13 09:11:41', 7);

-- --------------------------------------------------------

--
-- Table structure for table `driver_master`
--

CREATE TABLE `driver_master` (
  `id` bigint(20) NOT NULL,
  `driver_name` varchar(255) DEFAULT NULL,
  `customer_name` varchar(200) NOT NULL,
  `current_latitude` varchar(255) DEFAULT NULL,
  `current_longitude` varchar(255) DEFAULT NULL,
  `distance` float NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `updated_on` datetime
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_master`
--

INSERT INTO `driver_master` (`id`, `driver_name`, `customer_name`, `current_latitude`, `current_longitude`, `distance`, `status`, `updated_on`) VALUES
(1, 'Driver 1', '', '19.112214', '72.915240', 0, 'Available', '2020-10-13 08:45:29'),
(2, 'Driver 2', '', '19.109933', '72.902832', 0, 'Available', '2020-10-10 13:25:42'),
(3, 'Driver 3', '', '19.127308', '72.930620', 0, 'Available', '2020-10-13 08:45:05'),
(4, 'Driver 4', '', '19.130560', '72.922856', 0, 'Available', '2020-10-13 08:40:59'),
(5, 'Driver 5', '', '19.114659', '72.912324', 0, 'Available', '2020-10-13 08:40:55'),
(6, 'Driver 6', '', '19.126554', '72.873689', 0, 'Available', '2020-10-13 09:08:14'),
(7, 'Driver 7', 'Suraj Shingade', '19.137367', '72.852037', 0, 'Busy', '2020-10-13 09:11:41'),
(8, 'Driver 8', '', '19.156404', '72.862926', 0, 'Available', '2020-10-13 08:40:22'),
(9, 'Driver 9', '', '19.163171', '72.963308', 0, 'Available', '2020-10-13 08:49:50'),
(10, 'Driver 10', '', '18.988127', '72.852844', 0, 'Available', '2020-10-13 08:50:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking_master`
--
ALTER TABLE `booking_master`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_booking_master_allocated_driver_id` (`allocated_driver_id`);

--
-- Indexes for table `driver_master`
--
ALTER TABLE `driver_master`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking_master`
--
ALTER TABLE `booking_master`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `driver_master`
--
ALTER TABLE `driver_master`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking_master`
--
ALTER TABLE `booking_master`
  ADD CONSTRAINT `fk_booking_master_allocated_driver_id` FOREIGN KEY (`allocated_driver_id`) REFERENCES `driver_master` (`id`);
COMMIT;

