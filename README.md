# LogiNextProgrammingChallenge
Prepared By Suraj Shingade

Things used :

Spring Boot 2.2.7.RELEASE
HTML Jquery
Google Maps ( Without API Key, Dev Use Only)
MYSQL ( Schema Attached ddl/v1.sql)

OpenJDK 11

To start your backend application in the dev profile, run:

```
./mvnw
```
To start your front application in the local, run:
```

cd ui &&  npm intall
npm start
```

#Sample Co-ordinates : 
lat , lng
19.1246111,72.8418946
19.1377411,72.8588063

